//
// Created by Boris on 04/06/2019.
//

#include <Display.h>
#include <Debug.h>

// create LCD screen instance - note that pin numbers differ from library defaults so have to set them explicity in the constructor here
Display::Display() : lcd(LCD_ADDR, EN_pin, RW_pin, RS_pin, D4_pin, D5_pin, D6_pin, D7_pin) {

    uint8_t customChar[2][8] = {
            {0x1f, 0x11, 0x11, 0x11, 0x11, 0x11, 0x1f},        // open square, position 0
            {0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f}        // filled square, position 1
    };

    // LCD initialization
    lcd.begin(LCD_COLS, LCD_ROWS);
    lcd.setBacklightPin(BACKLIGHT_pin, POSITIVE);
    lcd.setBacklight(HIGH);

    DEBUG("LCD backlight set to high\r\n");

    lcd.createChar(0, (uint8_t *) customChar[0]);        // special characters for setpoint status
    lcd.createChar(1, (uint8_t *) customChar[1]);
}

void Display::startup() {
    char buffer[21];

    lcd.clear();
    lcd.setCursor(0, 0);
    sprintf_P(buffer, PSTR("BBQ v%s"), VERSION);
    DEBUG("%s\r\n", buffer);
    lcd.print(buffer);
    lcd.setCursor(0, 1);
    lcd.print(F(__DATE__));
    lcd.setCursor(12, 1);
    lcd.print(F(__TIME__));
    lcd.setCursor(0, 3);
    lcd.print(F("Booting ..."));
    DEBUG("Done LCD startup\r\n");
}

void Display::connectingWifi(const char* ssid) {
}

void Display::wifiConnectionFailed() {
}

void Display::wifiConnected() {
}

void Display::connected() {
    lcd.clear();
    lcd.setCursor(5, 1);
    lcd.print(F("Connected"));
}

void Display::displaySetup(uint8_t probeCount) {
    lcd.clear();

    char buffer[21];
    for (int i = 0; i < probeCount; i++) {
        sprintf_P(buffer, PSTR("P%d Tmp:"), i + 1);
        lcd.setCursor(0, i);
        lcd.print(buffer);
    }
}

void Display::setTemp(TempProbe *probe) {
    String temp(probe->temp(), 2);
    lcd.setCursor(7, probe->pin);
    lcd.print(temp);
}

void Display::notPresent(TempProbe *probe) {
    lcd.setCursor(3, probe->pin);
    lcd.print(F("No Probe         "));
}
