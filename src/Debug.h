//
// Created by Boris on 07/06/2019.
//

#ifndef THERMOMETER_DEBUG_H
#define THERMOMETER_DEBUG_H

#ifdef DEBUG_ESP_PORT

#include <Arduino.h>

#define DEBUG(...) DEBUG_ESP_PORT.printf( __VA_ARGS__ )

#else

#define DEBUG(...)

#endif

#endif //THERMOMETER_DEBUG_H
