/*
   Portions of the code are copyright 2016 Rob Redford
   This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
   To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.

   Portions of the thermistor code are Copyright 2013 Tony DiCola (tony@tonydicola.com).
   Released under an MIT license: http://opensource.org/licenses/MIT
*/
#include <Arduino.h>

#include <Debug.h>

#include <ESP8266WiFi.h>
#include <ESPAsyncDNSServer.h>
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>
#include <ESP8266httpUpdate.h>

#include <MCP320x.h>
#include <TempProbe.h>

#include <SimpleTimer.h>

#include <Display.h>

#define PROBE_COUNT 4
// Measured series resistor value in ohms. Must be the SAME as the nominal 25deg C resistance of the probe
static const uint32_t seriesResistor[PROBE_COUNT] = {200000, 200000, 200000, 200000};

#define PROBE_DELAY 1000
#define LCD_DELAY 3000 // slow enough to minimize flicker
#define FIRMWARE_DELAY 300000
#define PUT_DATA_DELAY 10000

#define SSID "Barbeque autconf"

MCP320x mcp(SS); // see pins_arduino.h for ESP8266/Huzzah - using only 1 parameter forces SPI MODE (uses SPI library)
SimpleTimer timer;
Display display;
// Measured series resistor value in ohms. Must be the SAME as the nominal 25deg C resistance of the probe
TempProbe *probes[PROBE_COUNT];

bool redrawScreen = false;

void updateProbes();

void updateLCD();

void updateFirmware();

void putTemperatureResults();

void setup() {
    Serial.begin(115200);
    while (!Serial);

    display.startup();
    delay(2000);
    DEBUG("Connecting to WiFi.\r\n");

    AsyncWebServer server(80);
    AsyncDNSServer dns;
    AsyncWiFiManager wifiManager(&server, &dns);

#ifdef RESET_WIFI
    DEBUG_LINE("Reset WiFi configuration");
    wifiManager.resetSettings();
#endif

    display.connectingWifi(SSID);
    delay(2000);

    if (!wifiManager.autoConnect(SSID)) {
        display.wifiConnectionFailed();
        DEBUG("WiFi configuration failed. Reboot.\r\n");
        delay(2000);
        ESP.reset();
        delay(1000);
    }

    display.wifiConnected();
    DEBUG("Connected to WiFi!\r\n");
    delay(2000);

    mcp.setMCPConfig(MCP_SINGLE, MCP_ALL_PORTS);            // single mode for all MPC3204 pins
    DEBUG("mcp configured\r\n");

    display.connected();

    for (uint8_t i = 0; i < PROBE_COUNT; i++) {
        probes[i] = new TempProbe(&mcp, i, seriesResistor[i]);
        DEBUG("Setting up probe %d\r\n", probes[i] -> pin);
        probes[i] ->updateProbe();
    }
    DEBUG("probes configured\r\n");

    timer.setInterval(PROBE_DELAY, updateProbes);
    timer.setInterval(LCD_DELAY, updateLCD);
    timer.setInterval(FIRMWARE_DELAY, updateFirmware);
    timer.setInterval(PUT_DATA_DELAY, putTemperatureResults);
    DEBUG("Timers started\r\n");

    display.displaySetup(PROBE_COUNT);
}

void loop() {
    timer.run();
}

/*
  Get current probe temps
  Check for probes that have been disconnected or reconnected
*/
void updateProbes() {
    for (auto &probe : probes) {
        DEBUG("Updating probe %d\r\n", probe->pin);
        if (probe->updateProbe()) {
            DEBUG("Probe %d state changed, redraw screen\r\n", probe->pin);
            redrawScreen = true;
        }
    }
}

/*
 Update the physical LCD display
*/
void updateLCD() {
    if (redrawScreen) {
        display.displaySetup(PROBE_COUNT);
        redrawScreen = false;
    }
    for (auto &probe : probes) {
        if (probe->isPresent()) {
            display.setTemp(probe);
        } else {
            display.notPresent(probe);
        }
    }
}

/*
 Update the firmware from the update server
 */
void updateFirmware() {
    WiFiClient client;
    HTTPUpdateResult ret = ESPhttpUpdate.update(client, OTA_SERVER, VERSION);
    switch (ret) {
        case HTTP_UPDATE_FAILED:
            DEBUG("HTTP update failed!\r\n");
            break;
        case HTTP_UPDATE_NO_UPDATES:
            DEBUG("HTTP update not required.\r\n");
            break;
        case HTTP_UPDATE_OK:
            DEBUG("HTTP update succeeded! Reboot.\r\n");
            break;
    }
}

void putTemperatureResults() {
    WiFiClient client;
    HTTPClient http;

    http.begin(client, TEMPERATURE_SERVER);
    http.addHeader("Content-Type", "text/plain");

    String data;
    for (auto &probe : probes) {
        data += ";";
        data += probe->pin;
        data += probe->isPresent() ? 1 : 0;
        data += String(probe->temp(), 2);
    }
    DEBUG("PUT temperature data request '%s'.\r\n", data.c_str());
    int httpCode = http.PUT(data);
    DEBUG("PUT temperature data response HTTP%d.\r\n", httpCode);
    http.end();
}