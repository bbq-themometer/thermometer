//
// Created by Boris on 02/06/2019.
//

#include <Debug.h>
#include <TempProbe.h>

TempProbe::TempProbe(MCP320x *mcp, uint8_t pin, float seriesResistor) : pin(pin), mcp(mcp), seriesResistor(seriesResistor), currentTemp(0), present(false) {}

const float TempProbe::temp() {
    return currentTemp;
}

const bool TempProbe::isPresent() {
    return present;
}

bool TempProbe::updateProbe() {
    float temp = readTemp();

#if defined(DEBUG_ESP_PORT)
    float raw = probeRaw();
    DEBUG("%d: %f. T: %f. V: %f.\r\n", pin, raw, temp, mcp->rawToVoltage(3.3, raw));
#endif

    if (std::abs(temp - currentTemp) < TOLERANCE) {
        return false;
    }

    currentTemp = temp;

    if (std::isnan(currentTemp) && present) {
        /*
           probe disconnected:
        */
        DEBUG("Probe was present, now disconnected. Present: %d\r\n", present);
        present = false;
        return true;
    }
    if (!std::isnan(currentTemp) && !present) {
        /*
         probe connected:
        */
        DEBUG("Probe was disconnected, now present. Present: %d\r\n", present);
        present = true;
        return true;
    }
    return false;
}

/*
   Derive the resistance of the probe to use in the Steinhart-Hart eqution.
   We can do this given the known value of the series resistor in the fixed part of the voltage divider
   and the fact that since V = I*R the voltage drop on each resistor is directly proportional to voltage.
   Thus, we can use a ratio to determine the resistance of the probe without using the reference voltage which can vary considerably from 5V.
   Also, this allows us to use the same code on 5V or 3.3V platforms :-) as well as just working with the digitized reading without any voltage conversions.

   NOTE: both float and double on Arduino are 32-bit values. No difference between the two.
*/
float TempProbe::calcResistance() {
    float reading = 0;
    for (uint8_t i = 0; i < ADC_SAMPLES; i++) {
        reading += mcp->readChannel(pin);
    }
    if (reading <= (ADC_ZERO_THRESHOLD * ADC_SAMPLES)) {
        // probe disconnected
        return NAN;
    }
    reading /= (float) ADC_SAMPLES;
    reading = (ADC_RESOLUTION / reading) - 1;
    return seriesResistor / reading;

}

/*
  get the normalized raw value of the probe on the given pin for debug
  note that a disconnected probe will return 0
*/
float TempProbe::probeRaw() {
    float reading = 0;
    for (uint8_t i = 0; i < ADC_SAMPLES; i++) {
        reading += mcp->readChannel(pin);
    }
    reading /= ADC_SAMPLES;
    return reading;
}

/*
   read the temperature from the given analog pin and return temperature in degrees F
   uses the Steinhart-Hart equation
   Note: when using constants in float calculations, make sure there is a decimal point
*/
float TempProbe::readTemp() {
    const float R = calcResistance();
    if (std::isnan(R)) {
        // probe disconnected
        return NAN;
    }
    const float kelvin = 1.0 / (A + B * log(R) + C * pow(log(R), 3.0));
    return kelvin - ABSOLUTE_ZERO;
}