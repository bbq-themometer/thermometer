//
// Created by Boris on 02/06/2019.
//

#ifndef THERMOMETER_DISPLAY_H
#define THERMOMETER_DISPLAY_H

#include <TempProbe.h>

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

/*
   pins on the I2C backpack on the LCD
   these pins are not exposed to the Arduino but are necessary to define for the library
   (see class instantiation below)
   pin definitions don't match schematics - extracted from http://arduino.arigato.cz/I2C_LCD_BackPack_for_1602_and_2004/LCD_16x2_BackPack_v003.ino
*/
#define BACKLIGHT_pin 3
#define EN_pin 2
#define RW_pin 1
#define RS_pin 0
#define D4_pin 4
#define D5_pin 5
#define D6_pin 6
#define D7_pin 7

/*
   LCD defines
*/
#define LCD_ADDR 0x27         // SainSmart 2004. See I2C scanner utility in Test folder
#define LCD_ROWS 4
#define LCD_COLS 20

class Display {
public:
    Display();

    void displaySetup(uint8_t probeCount);

    void startup();

    void connectingWifi(const char* ssid);

    void wifiConnectionFailed();

    void wifiConnected();

    void connected();

    void setTemp(TempProbe *probe);

    void notPresent(TempProbe *probe);

private:
    LiquidCrystal_I2C lcd;
};

#endif //THERMOMETER_DISPLAY_H
