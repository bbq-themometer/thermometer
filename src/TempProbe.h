//
// Created by Boris on 02/06/2019.
//

#ifndef THERMOMETER_TEMPPROBE_H
#define THERMOMETER_TEMPPROBE_H

#if defined(ARDUINO) && (ARDUINO >= 10605)

#include "Arduino.h"

#else
#error This library requires Arduino IDE 1.6.5 or above
#endif

#include "MCP320x.h"

#define  ADC_ZERO_THRESHOLD 10            // threshold value for a zero reading (capacitance?)
#define ADC_SAMPLES 5
#define TOLERANCE 0.001

/*
   Steinhart-Hart coefficients obtained empirically from the calibration utility for the Maverik ET-73 200k ohm probe
   See https://en.wikipedia.org/wiki/Steinhart-Hart for reference and readTemp() below
*/
#define A 0.001433954477
#define B 0.000076877069
#define C 0.000000537597

#define ABSOLUTE_ZERO 273.15

#define ADC_RESOLUTION 4095          // ADC resolution: depends on the platform. Mega/Uno is 10 bits, so (2^10)-1 = 1023. This is for the 12-bit MPC3204 SPI ADC

class TempProbe {
public:
    const uint8_t pin;

    TempProbe(MCP320x *mcp, uint8_t pin, float seriesResistor);

    const bool isPresent();

    const float temp();

    bool updateProbe();

private:
    TempProbe(const TempProbe&);
    TempProbe& operator=(const TempProbe&);

    MCP320x *mcp;
    const float seriesResistor;
    float currentTemp;
    bool present;

    float calcResistance();

    float probeRaw();

    float readTemp();
};

#endif //THERMOMETER_TEMPPROBE_H
